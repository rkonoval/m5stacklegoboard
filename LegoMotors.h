/*
  LegoMotors.h - Library to operate m5stack lego motors module
  Created by Roman Konoval, 2019.
  Released into the public domain.
*/

#ifndef LEGOMOTORS_h
#define LEGOMOTORS_h

#include <Arduino.h>

#define DEBUG_ON true

class LegoMotors {
  public:
    void init(bool debug);
    void setSpeed(byte motor, int16_t sp);
    int16_t getSpeed(byte motor);
    uint8_t* getRegisters();
  private:
    void readAddress(int address, int count, void* result);
    void writeAddress(int address, int count, void* value);

    uint8_t registers[32];
    bool debug;
};

#endif

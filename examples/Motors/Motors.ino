
#include <M5Stack.h>
#include "LegoMotors.h"

void initLcd() {
  M5.Lcd.clear(BLACK);
  M5.Lcd.setTextColor(YELLOW);
  M5.Lcd.setTextSize(2);  
}

String toHex(uint8_t c) {
  if (c<0x10) {
    return String("0") + String(c, HEX);
  } else {
    return String(c, HEX);
  }
}

void showMotor(byte motor) {
  M5.Lcd.setTextSize(2);
  M5.Lcd.setCursor(60, 220);
  M5.Lcd.print(String(motor));
}

void showSpeed(int speed) {
  M5.Lcd.setTextSize(2);
  M5.Lcd.setCursor(140, 220);
  M5.Lcd.print(String(speed, DEC));
  M5.Lcd.print("   ");
  
}

void showBytes(uint8_t* bytes) {
  M5.Lcd.setTextSize(2);
  M5.Lcd.setCursor(0, 0);
  int i = 0;
  //for(int i=0;i<4;++i) {
    //M5.Lcd.print(toHex(i*8) + ":");
    for(int j=0;j<8;++j) {
      M5.Lcd.print(toHex(bytes[i*8+j]) + " ");
    }
    M5.Lcd.println();
  //}
}


LegoMotors motors = LegoMotors();
int current_motor = 3;


void setup() {
  motors.init(DEBUG_ON);
  M5.begin(true, false, true);
  refreshMem();
}

void refreshMem() {
  showBytes(motors.getRegisters());
  showMotor(current_motor);
  showSpeed(motors.getSpeed(current_motor));
}

void loop() {
  M5.update();

  if (M5.BtnA.wasReleased()) {
    current_motor += 1;
    current_motor %= 4;
    refreshMem();
  }

  if (M5.BtnB.wasReleased()) {
    int sp = motors.getSpeed(current_motor);
    if (sp == 0) {
      sp = 255;
    } else if (sp == 255) {
      sp = -255;
    } else {
      sp = 0;
    }
    motors.setSpeed(current_motor, sp);
    refreshMem();
  }

}

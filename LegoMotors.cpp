#include "LegoMotors.h"
#include <Wire.h>

#define LEGO_ADDR          0x56

#define FIRWMARE_VER_ADDR   0x64
#define I2C_SET_ADDR        0x63


void LegoMotors::init(bool debug) {
  Wire.begin();
  this->debug = debug;
}

void LegoMotors::setSpeed(byte motor, int16_t speed) {
  if (debug) {
    Serial.print("LegoMotors: setSpeed: ");
    Serial.print(motor, DEC);
    Serial.print(": ");
    Serial.println(speed, DEC);
  }
  writeAddress(motor * 2, 2, &speed);
}

int16_t LegoMotors::getSpeed(byte motor) {
  int16_t current_speed;
  readAddress(motor * 2, 2, &current_speed);
  return current_speed;
}

uint8_t* LegoMotors::getRegisters() {
  for(int m=0;m<4;++m) {
    readAddress(m*2, 2, registers+m*2);
  }
  /*
  for(int e=0;e<4;++e) {
    readAddress(8+e*4, 4, registers+8+e*4);
  }*/
  return registers;
}

void LegoMotors::readAddress(int address, int count, void* result) {
  Wire.beginTransmission(LEGO_ADDR);
  Wire.write(byte(address));
  Wire.endTransmission();

  Wire.requestFrom(LEGO_ADDR, count);

  for(int i=0; i<count; ++i) {
    char c = Wire.read();
    ((uint8_t*)result)[i] = (uint8_t)c;
  }
}

void LegoMotors::writeAddress(int address, int count, void* value) {
  Wire.beginTransmission(LEGO_ADDR);
  Wire.write((uint8_t)address);
  Wire.write((uint8_t*)value, count);
  Wire.endTransmission();
}


